import React, { Component } from "react";
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  withStyles,
} from "@material-ui/core";
import styles from "./styles";

class MultipleChoice extends Component {
  handleChange = (event) => {
    const selectedAnswer = this.props.item.answers.find((item) => {
      return item.id === event.target.value;
    });

    const data = {
      questionId: this.props.item.id,
      answer: {
        content: selectedAnswer.content,
        exact: selectedAnswer.exact,
      },
    };

    this.props.handleQuestionChange(event, this.props.item.questionType, data);
  };

  render() {
    const { classes } = this.props;

    return (
      <FormControl
        fullWidth
        component="fieldset"
        className={classes.formControl}
      >
        <FormLabel component="legend" focused={false} className={classes.label}>
          Câu hỏi {this.props.index}: {this.props.item.content}
        </FormLabel>
        <RadioGroup
          name={`answer-${this.props.item.id}`}
          value={this.props.value}
          onChange={this.handleChange}
        >
          {this.props.item.answers.map((item) => {
            return (
              <FormControlLabel
                key={item.id}
                value={item.id}
                control={<Radio color="primary" />}
                label={item.content}
              />
            );
          })}
        </RadioGroup>
      </FormControl>
    );
  }
}

export default withStyles(styles)(MultipleChoice);
