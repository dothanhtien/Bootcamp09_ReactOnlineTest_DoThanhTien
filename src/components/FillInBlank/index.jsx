import React, { Component } from "react";
import {
  FormGroup,
  TextField,
  Typography,
  withStyles,
} from "@material-ui/core";
import styles from "./styles";

class FillInBlank extends Component {
  checkExact = (value) => {
    return (
      this.props.item.answers[0].content.toLowerCase() === value.toLowerCase()
    );
  };

  handleChange = (event) => {
    const data = {
      questionId: this.props.item.id,
      answer: {
        content: event.target.value,
        exact: this.checkExact(event.target.value),
      },
    };
    this.props.handleQuestionChange(event, this.props.item.questionType, data);
  };

  render() {
    const { classes } = this.props;

    return (
      <FormGroup className={classes.formGroup}>
        <Typography
          variant="inherit"
          component="label"
          className={classes.label}
        >
          Câu hỏi {this.props.index}: {this.props.item.content}
        </Typography>
        <TextField
          name={`answer-${this.props.item.id}`}
          placeholder="Nhập câu trả lời"
          fullWidth
          margin="normal"
          variant="outlined"
          value={this.props.value}
          onChange={this.handleChange}
        />
      </FormGroup>
    );
  }
}

export default withStyles(styles)(FillInBlank);
