const styles = () => {
  return {
    formGroup: {
      marginBottom: "1rem",
    },
    label: {
      fontWeight: 700,
    },
  };
};

export default styles;
