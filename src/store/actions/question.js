import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";

export const getQuestionList = async (dispatch) => {
  try {
    const res = await axios({
      method: "GET",
      url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/questions",
    });
    dispatch(createAction(actionType.SET_QUESTION_LIST, res.data));
  } catch (err) {
    console.log(err);
  }
};

export const sendAnswer = (answer) => {
  return (dispatch) => {
    dispatch(createAction(actionType.SET_ANSWER_LIST, answer));
  };
};

export const cleanupAnswerList = (dispatch) => {
  dispatch(createAction(actionType.CLEAN_UP_ANSWER_LIST));
};
