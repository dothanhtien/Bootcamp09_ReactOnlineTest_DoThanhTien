import React, { Component } from "react";
import {
  Box,
  Button,
  CircularProgress,
  Container,
  Typography,
} from "@material-ui/core";
import MultipleChoice from "../../components/MultipleChoice";
import FillInBlank from "../../components/FillInBlank";
import ResultModal from "../../components/ResultModal";
import { connect } from "react-redux";
import {
  cleanupAnswerList,
  getQuestionList,
  sendAnswer,
} from "../../store/actions/question";

let typingTimeout = null;

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      score: 0,
      questionValues: {},
      openDialog: false,
    };
  }

  componentDidMount() {
    this.props.dispatch(getQuestionList);
  }

  renderQuestionList = () => {
    if (!this.props.questionList.length) {
      return (
        <Box align="center">
          <CircularProgress />
          <Typography variant="h6" component="h6">
            Đang tải...
          </Typography>
        </Box>
      );
    }

    return this.props.questionList.map((item, index) => {
      let questionHTML = "";

      if (item.questionType === 1) {
        questionHTML = (
          <MultipleChoice
            index={+index + 1}
            key={item.id}
            item={item}
            handleQuestionChange={this.handleQuestionChange}
            value={this.state.questionValues[`answer-${item.id}`] || ""}
          />
        );
      }
      if (item.questionType === 2) {
        questionHTML = (
          <FillInBlank
            index={+index + 1}
            key={item.id}
            item={item}
            handleQuestionChange={this.handleQuestionChange}
            value={this.state.questionValues[`answer-${item.id}`] || ""}
          />
        );
      }

      return questionHTML;
    });
  };

  handleQuestionChange = (event, questionType, data) => {
    this.setState({
      questionValues: {
        ...this.state.questionValues,
        [event.target.name]: event.target.value,
      },
    });

    // nếu là multiple choice, dispatch kết quả ngay lên store
    if (questionType === 1) {
      this.props.dispatch(sendAnswer(data));
    }

    // mếu là fill in blank, đợi người dùng nhấn xong 0.3s sẽ dispatch kết quả lên store
    if (questionType === 2) {
      if (typingTimeout) {
        clearTimeout(typingTimeout);
      }

      typingTimeout = setTimeout(() => {
        this.props.dispatch(sendAnswer(data));
      }, 300);
    }
  };

  handleSubmit = () => {
    const score = this.props.answerList.filter((item) => {
      return item.answer.exact;
    }).length;

    this.setState({ score });

    this.handleOpenDialog();
  };

  handleOpenDialog = () => {
    this.setState({ openDialog: true });
  };

  handleCloseDialog = () => {
    this.setState({ openDialog: false });

    // clean up
    //  1. reset điểm
    //  2. reset các câu trả lời đã điền trước đó
    //  3. reset answerList trên store
    this.setState({
      score: 0,
      questionValues: {},
    });
    this.props.dispatch(cleanupAnswerList);
  };

  render() {
    return (
      <Container maxWidth="md">
        <Typography
          variant="h3"
          component="h1"
          align="center"
          style={{ margin: "1rem 0" }}
        >
          Online Test
        </Typography>

        {this.renderQuestionList()}

        {!!this.props.questionList.length && (
          <Button
            variant="contained"
            color="primary"
            style={{ marginBottom: "1rem" }}
            onClick={this.handleSubmit}
          >
            Nộp bài
          </Button>
        )}

        <ResultModal
          open={this.state.openDialog}
          title={"Kết quả"}
          handleCloseDialog={this.handleCloseDialog}
        >
          {`Bạn đã hoàn thành bài kiểm tra với số điểm đạt được là ${this.state.score}`}
        </ResultModal>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    questionList: state.question.questionList,
    answerList: state.question.answerList,
  };
};

export default connect(mapStateToProps)(Home);
