const styles = () => {
  return {
    formControl: {
      marginBottom: "1rem",
    },
    label: {
      fontWeight: 700,
      color: "#000000",
    },
  };
};

export default styles;
