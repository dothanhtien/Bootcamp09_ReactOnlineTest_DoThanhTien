import { actionType } from "../actions/type";

const initialState = {
  questionList: [],
  answerList: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_QUESTION_LIST: {
      state.questionList = action.payload;
      return { ...state };
    }
    case actionType.SET_ANSWER_LIST: {
      const cloneAnswerList = [...state.answerList];

      const foundIndex = cloneAnswerList.findIndex((item) => {
        return item.questionId === action.payload.questionId;
      });

      if (foundIndex === -1) {
        cloneAnswerList.push(action.payload);
      } else {
        cloneAnswerList[foundIndex] = action.payload;
      }

      state.answerList = cloneAnswerList;

      return { ...state };
    }
    case actionType.CLEAN_UP_ANSWER_LIST: {
      let cloneAnswerList = [...state.answerList];
      cloneAnswerList = [];
      state.answerList = cloneAnswerList;

      return { ...state };
    }
    default:
      return state;
  }
};

export default reducer;
